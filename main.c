#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int get_int_data()
{
    /*
     * From StackOverflow
     * Which is the best way to get input from user in C
     */
    char line[7];
    int  data;
    int  keep_looping = 1;

    while (keep_looping) {
        if (fgets(line, sizeof(line), stdin)) {
            if (1 == sscanf(line, "%i", &data))
                keep_looping = 0;
        }
    }

    return data;
}

void print_array(int array[], int size)
{
    for (int i = 0; i < size; i++)
        printf("%i ", array[i]);
    puts("");
}

/* Sort and merge */
void post_merge_sort(int array_original[], int begin, int middle, int end, int array_copy[])
{
    int i = begin;
    int j = middle;

    for (int k = begin; k < end; k++) {
        if (i < middle && (j >= end || array_original[i] <= array_original[j])) {
            array_copy[k] = array_original[i];
            i++;
        } else {
            array_copy[k] = array_original[j];
            j++;
        }
    }

//    print_array(array_copy, end);
}

int recursion = 0;
/* Split array/2 recursively */
void pre_merge_sort(int array_copy[], int begin, int end, int array_original[])
{
    if (end - begin <= 1)
        return;

    recursion++;
    int middle = (end + begin) / 2;
    pre_merge_sort(array_original, begin , middle, array_copy);
    pre_merge_sort(array_original, middle, end   , array_copy);

    /* For pen & paper clarity on how this algorithm is being executed */
    printf("\nTimes function recursed: %i\n", --recursion);
    printf("\tBegin  : %i\n", begin);
    printf("\tMiddle : %i\n", middle);
    printf("\tEnd    : %i\n", end);

    post_merge_sort(array_copy, begin, middle, end, array_original);
}

void copy_array(int array_original[], int array_copy[], int size)
{
    for (int i = 0; i < size; i++)
        array_copy[i] = array_original[i];
}

/* Top down merge algorithm */
void merge_sort(int array_original[], int array_copy[], int size)
{
    copy_array(array_original, array_copy, size);

    pre_merge_sort(array_copy, 0, size, array_original);
}

int main()
{
    srand(time(NULL));

    printf("Enter array size: ");
    int size = get_int_data();
    printf("size: %i\n", size);

    while (size == 0 || size < 0) {
        puts("");
        puts("Number cannot be 0 or -ve.");
        printf("Try another number: ");
        size = get_int_data();
    }

    int array_original[size];
    for (int i = 0; i < size; i++)
        array_original[i] = rand() % 10;

    puts("Before sorting:");
    print_array(array_original, size);

    int array_copy[size];
    merge_sort(array_original, array_copy, size);

    puts("");
    puts("After sorting:");
    print_array(array_original, size);

    return 0;
}
